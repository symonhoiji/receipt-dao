package com.mycompany.product_conponent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Symon
 */
public class TestReceipt {
    public static void main(String[] args) {
        ProductPojo p1 = new ProductPojo(1, "dsad", 30);
        ProductPojo p2 = new ProductPojo(2, "Americano", 30);
        User seller = new User("Naded","88888888", "123456");
        Customer customer = new Customer("Pawit", "888888");
        Receipt receipt = new Receipt(seller, customer);
        
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        
        System.out.println(receipt);
        
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
    }
    
}
