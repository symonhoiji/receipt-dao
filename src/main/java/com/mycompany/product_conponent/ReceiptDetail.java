/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.product_conponent;

/**
 *
 * @author Symon
 */
public class ReceiptDetail {
    private int id;
    private ProductPojo product;
    private double price;
    private int amount;
    Receipt receipt;

    public ReceiptDetail(int id, ProductPojo product, int amount,double price, Receipt receipt) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.receipt = receipt;
    }
    
    public ReceiptDetail(ProductPojo product, int amount,double price, Receipt receipt) {
        this(-1,product,amount,price,receipt);
    }
    public double getTotal(){
        return amount*price;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductPojo getProduct() {
        return product;
    }

    public void setProduct(ProductPojo product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }
public void addAmount(int amount){
        this.amount = this.amount + amount;
    }
    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + 
                ", product=" + product + 
                ", price=" + price + 
                ", amount=" + amount  + 
                ", total=" + this.getTotal() + '}';
    }

    
    
}
