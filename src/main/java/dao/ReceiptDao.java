/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.product_conponent.Customer;
import com.mycompany.product_conponent.ProductPojo;
import database.Database;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import com.mycompany.product_conponent.Receipt;
import com.mycompany.product_conponent.ReceiptDetail;
import com.mycompany.product_conponent.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kanny
 */
public class ReceiptDao implements DaoInterface<Receipt>{

    private int id;

    @Override
    public int add(Receipt object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "INSERT INTO receipt (customer_id,user_id,total)VALUES (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();            
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                    id = result.getInt(1);
                    object.setId(id);
                }
            for(ReceiptDetail rd: object.getReceiptDetail()){
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,product_id,price,amount) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
                stmtDetail.setInt(1,rd.getReceipt().getId());
                stmtDetail.setInt(2, rd.getProduct().getId());
                stmtDetail.setDouble(3, rd.getPrice());
                stmtDetail.setInt(4,rd.getAmount());
                int rowDetail = stmtDetail.executeUpdate();            
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if(resultDetail.next()){
                    id = resultDetail.getInt(1);
                    rd.setId(id);
                }
            }
            
            
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return -1;
       
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
 
        try {
            
            String sql = "SELECT r.id as id,created,customer_id,user_id,total,c.tel as customer_tel,c.name as customer_name,u.name as user_name,u.tel as user_tel FROM receipt r, customer c,user u WHERE r.customer_id = c.id AND r.user_id = u.id ORDER BY created DESC   ";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customer_id = result.getInt("customer_id");
                String customer_name = result.getString("customer_name");
                String customer_tel = result.getString("customer_tel");
                int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_tel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt r = new Receipt(id,created,
                        new User(user_id,user_name,user_tel),
                        new Customer(customer_id,customer_name,customer_tel));  
                list.add(r);
            }
        } catch (SQLException ex) {
            System.out.println("Error"+ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
 
        try {
            
            String sql = "SELECT r.id as id,created,customer_id,user_id,total,c.tel as customer_tel,c.name as customer_name,u.name as user_name,u.tel as user_tel FROM receipt r, customer c,user u WHERE r.customer_id = c.id AND r.user_id = u.id AND r.id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()){
                
                int pid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customer_id = result.getInt("customer_id");
                String customer_name = result.getString("customer_name");
                String customer_tel = result.getString("customer_tel");
                int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_tel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt r = new Receipt(pid,created,
                        new User(user_id,user_name,user_tel),
                        new Customer(customer_id,customer_name,customer_tel)); 
                
                getReceiptDetail(con, id, r);
                
                return r;
            }
        } catch (SQLException ex) {
            System.out.println("Error"+ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return null;
    }

    private void getReceiptDetail(Connection con, int id1, Receipt r) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n" +
                "       receipt_id,\n" +
                "       product_id,\n" +
                "       p.name as product_name,\n" +
                "       p.price as product_price\n"+
                "       rd.price as price,\n" +
                "       amount\n" +
                "  FROM receipt_detail rd, product p\n" +
                "  WHERE receipt_id = ? AND rd.product_id = p.id";
        PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id1);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while(resultDetail.next()){
            int rdid = resultDetail.getInt("id");
            int prodId = resultDetail.getInt("product_id");
            String prodName = resultDetail.getString("product_name");
            double prodPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            ProductPojo newProduct = new ProductPojo(prodId, prodName, prodPrice);
            r.addReceiptDetail(rdid,newProduct, amount,price);
        }
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row=0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();            
            
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        
        
        ///
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection con = null;
//        Database db = Database.getInstance();
//        con = db.getConnection();
//        int row;
//        try {
//            String sql = "UPDATE product SET  name = ?,price = ? WHERE id = ?";
//            PreparedStatement stmt = con.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();            
//            return row;
//        } catch (SQLException ex) {
//            System.out.println("Error");
//        }
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        ProductPojo p1 = new ProductPojo(1, "Americano", 40);
        ProductPojo p2 = new ProductPojo(2, "Espresso", 30);
        User seller = new User(1,"Weerasak Seanglers ","0123456789", "123");
        Customer customer = new Customer(1,"Nonkirito", "123456789");
        Receipt receipt = new Receipt(seller, customer);
        
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        
        ReceiptDao dao = new ReceiptDao();
        dao.add(receipt);
        System.out.println(dao.getAll());
        
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New : "+newReceipt);
    }
}
